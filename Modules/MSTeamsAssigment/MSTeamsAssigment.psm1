﻿function New-CustomChannel{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [string] $ChannelName,
        [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
        [String] $GroupId
    )
    Process {
       New-TeamChannel -GroupId $GroupId -DisplayName $ChannelName
       
    }
}

# Import-Csv C:\Users\benka\Desktop\school\OS_scripting\PowerShell_projext_1\powershell\newchannels.csv | Select-Object -Property ChannelName | ForEach-Object {$FullChannelName = $_.ChannelName ; [PSCustomObject]@{ChannelName = "$FullChannelName Ben Van de Vliet" ; GroupId = 'fd8ebd00-3ebb-492a-b9c1-5b2ab6fc1736'}}