﻿$credential = Get-Credential
$rows = Import-Csv "channels.csv"

Connect-MicrosoftTeams -Credential $credential

Get-TeamChannel -GroupId fd8ebd00-3ebb-492a-b9c1-5b2ab6fc1736 | Write-Output

foreach ($row in $rows){
   New-TeamChannel -GroupId fd8ebd00-3ebb-492a-b9c1-5b2ab6fc1736 -DisplayName Ben_VandeVliet_$($row.Displaynaam) -Description $row.Omschrijving
}